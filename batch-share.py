#!/usr/bin/env python3

import sys
import os
import argparse
from dotenv import load_dotenv
from os.path import dirname
from os.path import join

sys.path.insert(0, join(dirname(__file__), 'src'))

from nextcloud import NextCloud


def main():

	# Get arguments
	help_path = "Path to the folder, which contains folders that should be shared e.g. '/Test'"
	help_name = "Name of the person or group that should receive the share e.g. 'Admin'"
	help_share_type = "Share with person (0) or group (1)"
	help_permissions = "Permission id: sum of the share ids as defined in nextcloud api manual e.g. 31 for all permissions"

	parser = argparse.ArgumentParser()
	parser.add_argument('path', help=help_path, type=str)
	parser.add_argument('name', help=help_name, type=str)
	parser.add_argument('share_type', help=help_share_type, type=int)
	parser.add_argument('permissions', help=help_permissions, type=int)

	args = parser.parse_args()
	nc_path = args.path
	nc_name = args.name
	nc_share_type = args.share_type
	nc_permissions = args.permissions

	load_dotenv()

	nextcloud_url = "https://" + os.getenv('NEXTCLOUD_HOSTNAME') + ":443"
	nextcloud_username = os.getenv('NEXTCLOUD_USERNAME')
	nextcloud_password = os.getenv('NEXTCLOUD_PASSWORD')

	nxc = NextCloud(endpoint=nextcloud_url, user=nextcloud_username, password=nextcloud_password)

	folders_res = nxc.list_folders(path=nc_path)

	for folder in folders_res.data[1:]:
		folder = folder.href
		folder = folder.replace('/remote.php/dav/files/', '')
		f_cut = folder.find('/')
		folder = folder[f_cut:]
		print("Share: " + folder)
		share_res = nxc.create_share(path=folder, share_type=nc_share_type, share_with=nc_name, permissions=nc_permissions)
		print("Result: " + str(share_res))

	print("Done!")
	

if __name__ == "__main__":
	main()


