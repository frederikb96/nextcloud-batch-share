# Nextcloud-Scripts
Some scripts that might be useful to interact with the Nextcloud API.

## Batch-Share

This small script can be used to batch create shares in Nextcloud
- You have to put all the folders you want to share into one main directory
- The script will then share all the folders (only 1 level no subfolders) within this main directory
- You need to copy the temp.env file to .env and fill in your credentials
- Then execute the python script with the following parameters:

```
"Path to the folder, which contains folders that should be shared e.g. '/Test'"
"Name of the person or group that should receive the share e.g. 'Admin'"
"Share with person (0) or group (1)"
"Permission id: sum of the share ids as defined in nextcloud api manual e.g. 31 for all permissions"
```

## MvSharedPics

Also see the [Nextcloud-MvSharedPics App](https://gitlab.com/frederikb96/nextcloud-mvsharedpics). This python script is an alternative to the Nextcloud App, which is doing basically the same but only manually and via a different API. It was developed first, to test some concepts before creating the Nextcloud App. **Development is discontinued** in favor for the new Nextcloud App.
