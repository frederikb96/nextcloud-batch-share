#!/usr/bin/env python3

import sys
import os
import argparse
from dotenv import load_dotenv
from os.path import dirname
from os.path import join
import ast

sys.path.insert(0, join(dirname(__file__), 'src'))

from nextcloud import NextCloud


load_dotenv()

nextcloud_url = "https://" + os.getenv('NEXTCLOUD_HOSTNAME') + ":443"
share_folder = os.getenv('SHARE_FOLDER')
shared_pictures_folders = ast.literal_eval(os.getenv('SHARE_PICTURES_FOLDERS'))
users = ast.literal_eval(os.getenv('USERS'))


def share_fix_permissions(user, path_folder):

    print("Update share: " + path_folder)

    nxc = NextCloud(endpoint=nextcloud_url, user=user['name'], password=user['pw'])

    share_info = nxc.get_shares_from_path(path_folder)

    if not share_info.is_ok:
        raise RuntimeError("Share not valid!")

    share_id = share_info.data[0]['id']

    res_update = nxc.update_share(share_id, permissions=31)
    if not res_update.is_ok:
        print("Update not successful!")
    else:
        print("Updated!")


def share_move(users, folder_name):
    folder_path = os.path.join(share_folder, folder_name)

    for user in users:
        nxc = NextCloud(endpoint=nextcloud_url, user=user['name'], password=user['pw'])
        folder_exists = nxc.list_folders(path=folder_path, depth=0)
        if folder_exists.is_ok:
            folder_path_new = None
            for shared_picture_folder in shared_pictures_folders:
                folder_exists = nxc.list_folders(path=shared_picture_folder, depth=0)
                if folder_exists.is_ok:
                    folder_path_new = shared_picture_folder
                    break
            if not folder_path_new:
                break
            folder_path_new = os.path.join(folder_path_new, folder_name)
            print("User: ", user['name'])
            print("Move: " + folder_path + " to " + folder_path_new)
            res_move = nxc.move_path(folder_path, folder_path_new)
            if not res_move.is_ok:
                print("Move not successful!")
            else:
                print("Moved!")


def main():

    # Specify the user who created the share and the folder, which was shared

    # Change permissions of the share
    share_fix_permissions(users[1], "/Test")

    # Move the share
    share_move(users, "Test")

    print("Done!")


if __name__ == "__main__":
    main()

